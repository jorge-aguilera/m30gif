package simple.cqrs.event.client

import io.micronaut.context.event.ApplicationEventPublisher
import simple.cqrs.event.AbstractEvent

import javax.inject.Inject

class AppContextPublisher<T> implements EventPublisher<T>{

    @Inject
    ApplicationEventPublisher publisher

    @Override
    void publish(AbstractEvent<T> event) {
        publisher.publishEvent(event)
    }
}
