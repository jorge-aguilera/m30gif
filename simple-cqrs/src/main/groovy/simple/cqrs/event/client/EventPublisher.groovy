package simple.cqrs.event.client

import simple.cqrs.event.AbstractEvent

interface EventPublisher<T> {

    void publish(AbstractEvent<T> event)

}
