package simple.cqrs.event

abstract class AbstractEvent<T> {

    abstract String getEventId();

    abstract T getDtoFromEvent();

}
