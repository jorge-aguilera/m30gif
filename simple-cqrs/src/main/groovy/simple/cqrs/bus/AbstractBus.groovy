package simple.cqrs.bus

import simple.cqrs.model.Command
import simple.cqrs.model.CommandHandler
import simple.cqrs.model.Result

abstract class AbstractBus implements Bus{


    protected abstract Map<String, CommandHandler> getHandlers()

    @Override
    def <R> Result handleCommand(Command<R> command) {

        CommandHandler<Command<R>, R> handler = handlers[command.commandName]
        if( handler )
            return handler.handleCommand(command)

        null
    }

    @Override
    def <R> void registerHandlerCommand(Command<R> command, CommandHandler<Command<R>, R> handler) {
        handlers[command.commandName] = handler
    }
}
