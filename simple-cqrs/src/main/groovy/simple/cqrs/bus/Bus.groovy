package simple.cqrs.bus

import simple.cqrs.model.Command
import simple.cqrs.model.CommandHandler
import simple.cqrs.model.Result

interface Bus {

    public <R> Result<R> handleCommand(Command<R> command)

    public <R> void registerHandlerCommand(Command<R> command, CommandHandler<Command<R>, R> handler)

}