package simple.cqrs.model

interface CommandHandler<C extends Command<R>, R> {

    Result<R> handleCommand (Command<R> command);

}