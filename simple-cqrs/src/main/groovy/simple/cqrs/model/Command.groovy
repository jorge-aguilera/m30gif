package simple.cqrs.model

interface Command<T> {

    String getCommandName()

}