package simple.cqrs.commands

import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher
import simple.cqrs.model.Command
import simple.cqrs.model.CommandHandler
import simple.cqrs.model.Result


abstract class AbstractCommandHandler<T>  implements CommandHandler<Command<T>, T> {

    private EventPublisher<T> publisher

    AbstractCommandHandler(EventPublisher<T> publisher) {
        this.publisher = publisher;
    }

    @Override
    Result<T> handleCommand(Command<T> command) {
        T dto = getDto(command)
        AbstractEvent<T> event = buildEvent(dto)
        publish(event)
        buildResult(dto)
    }

    abstract T getDto(Command<T> command)

    abstract AbstractEvent<T> buildEvent(T dto)

    void publish(AbstractEvent<T> event) {
        if (event != null) {
            publisher.publish(event);
        }
    }

    abstract Result<T> buildResult(T dto)
}
