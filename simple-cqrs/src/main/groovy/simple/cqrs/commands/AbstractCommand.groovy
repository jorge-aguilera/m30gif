package simple.cqrs.commands

import simple.cqrs.model.Command

abstract class AbstractCommand<T> implements Command<T>{

    @Override
    String getCommandName() {
        this.class.simpleName
    }
}
