package m30.xml.extractor.business.services

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import io.micronaut.context.annotation.Value
import io.reactivex.Observable
import m30.xml.extractor.business.commands.NewPictureCommand
import m30.xml.extractor.business.model.M30Picture
import simple.cqrs.bus.Bus

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class XmlM30Extractor implements M30Extractor{

    @Value('${extractor.url}')
    String xmlUrl

    @Inject
    Bus bus

    @Override
    @CompileStatic(TypeCheckingMode.SKIP)
    Observable<String> retrieveImages() {
        Observable.create{ emitter->
            def Camaras = new XmlParser().parse(xmlUrl)
            Camaras.Camara.each{ camara ->
                emitter.onNext emitCamara(camara)
            }
            emitter.onComplete()
        }
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    String emitCamara( Node camara ){
        byte[] img = ("http://"+camara.URL.text()).toURL().bytes
        M30Picture m30Picture = new M30Picture(name: camara.Nombre.text(), image:  img, when: Calendar.instance )
        NewPictureCommand command = new NewPictureCommand(m30Picture)
        bus.handleCommand(command)
        m30Picture.id
    }

}
