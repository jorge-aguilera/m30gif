package m30.xml.extractor.business.event.client

import io.micronaut.context.annotation.Primary
import m30.xml.extractor.business.model.M30Picture
import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher

import javax.inject.Inject

@Primary
class KafkaPublisher implements EventPublisher<M30Picture>{

    @Inject
    EventClient<M30Picture> eventClient

    @Override
    void publish(AbstractEvent<M30Picture> event) {
        eventClient.sendEvent(event.eventId, event.dtoFromEvent)
    }
}
