package m30.xml.extractor.business.commands

import m30.xml.extractor.business.model.M30Picture
import simple.cqrs.commands.AbstractCommand

class NewPictureCommand extends AbstractCommand<M30Picture> {

    M30Picture picture

    NewPictureCommand(M30Picture picture){
        this.picture = picture
    }

}
