package m30.xml.extractor.business.event

import m30.xml.extractor.business.model.M30Picture
import simple.cqrs.event.AbstractEvent

class NewPictureEvent extends AbstractEvent<M30Picture> implements  Serializable{

    M30Picture picture

    NewPictureEvent(){
    }

    NewPictureEvent(M30Picture picture){
        this.picture = picture
    }

    @Override
    String getEventId() {
        picture.id
    }

    @Override
    M30Picture getDtoFromEvent() {
        picture
    }
}
