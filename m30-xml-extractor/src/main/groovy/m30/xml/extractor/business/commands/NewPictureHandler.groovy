package m30.xml.extractor.business.commands

import m30.xml.extractor.business.event.NewPictureEvent
import m30.xml.extractor.business.model.M30Picture
import m30.xml.extractor.business.model.M30PictureResult
import simple.cqrs.commands.AbstractCommandHandler
import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher
import simple.cqrs.model.Command
import simple.cqrs.model.Result

import javax.inject.Singleton

@Singleton
class NewPictureHandler extends AbstractCommandHandler<M30Picture>{

    NewPictureHandler(EventPublisher<M30Picture> eventPublisher){
        super(eventPublisher)
    }

    @Override
    M30Picture getDto(Command<M30Picture> command) {
        (command as NewPictureCommand).picture
    }

    @Override
    AbstractEvent<NewPictureCommand> buildEvent(M30Picture dto) {
        new NewPictureEvent(dto) as AbstractEvent
    }

    @Override
    Result<NewPictureCommand> buildResult(M30Picture dto) {
        new M30PictureResult() as Result
    }
}
