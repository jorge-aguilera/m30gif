package m30.xml.extractor.business.model

import groovy.transform.ToString

@ToString
class M30Picture implements Serializable{

    String name

    Calendar when

    byte[] image

    String getId(){
        "$name-$when.time.time"
    }

}
