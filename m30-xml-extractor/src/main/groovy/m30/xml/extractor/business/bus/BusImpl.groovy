package m30.xml.extractor.business.bus

import m30.xml.extractor.business.commands.NewPictureCommand
import m30.xml.extractor.business.model.M30Picture
import simple.cqrs.bus.AbstractBus
import simple.cqrs.model.Command
import simple.cqrs.model.CommandHandler

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BusImpl extends AbstractBus{

    Map<String, CommandHandler<Command<M30Picture>, M30Picture>> handlers = [:]


    @Inject
    BusImpl(CommandHandler<Command<M30Picture>, M30Picture> handler){
        handlers[NewPictureCommand.class.simpleName] = handler
    }

}
