package m30.xml.extractor.business.services

import io.reactivex.Observable

interface M30Extractor {

    Observable<String> retrieveImages()

}
