package m30.xml.extractor.business.services

import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Requires
import io.micronaut.context.env.Environment
import io.micronaut.scheduling.annotation.Scheduled

import javax.inject.Inject
import javax.inject.Singleton;

@Slf4j
@Singleton
@Requires(notEnv = Environment.TEST)
class JobExtractor {

    @Inject
    M30Extractor m30Extractor

    @Scheduled(fixedDelay = '${extractor.every}', initialDelay = '10s')
    void executeEveryTen() {
        m30Extractor.retrieveImages().doOnNext{ url ->
            log.debug url
        }.doOnComplete {
            log.debug "done"
        }.subscribe()
    }

}
