package m30.rest

import io.micronaut.context.annotation.Value
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Single

@Controller('/images')
class ImagesController {

    @Value('${m30.storage.path}')
    String storagePath

    @Get('/')
    Single<List<RestImage>>index(){
        Single.create{ emitter ->

            File root = new File(storagePath)
            root.mkdirs()
            def pattern = ~/.*\.gif/

            List<RestImage> list = []
            root.eachFileMatch pattern, {
                list.add( new RestImage(name:it.name.split('\\.').dropRight(1).join(''), id:it.name.md5() ) )
            }
            list.sort{ it.name }

            emitter.onSuccess(list)
        }
    }

    @Get(value='/{id}', produces = "image/gif")
    Single<byte[]>file(String id){

        Single.create{ emitter->
            File root = new File(storagePath)
            def pattern = ~/.*\.gif/

            File file
            root.eachFileMatch pattern, {
                if( it.name.md5() == id ){
                    file = it
                }
            }
            if( !file )
                emitter.onError(new FileNotFoundException())

            emitter.onSuccess(file.bytes)
        }

    }
}
