package m30.rest.business

import io.reactivex.Maybe
import m30.rest.business.model.M30Picture

interface StorageService {

    Maybe<M30Picture> save(M30Picture m30Picture)

}
