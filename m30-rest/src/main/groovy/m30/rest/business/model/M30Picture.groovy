package m30.rest.business.model


import groovy.transform.ToString

@ToString
class M30Picture {

    String name
    byte[] image
}
