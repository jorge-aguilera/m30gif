package m30.rest.business.event

import simple.cqrs.event.AbstractEvent

class NewGifEvent extends AbstractEvent<String>implements  Serializable{

    String picture

    NewGifEvent(){
    }

    NewGifEvent(String picture){
        this.picture = picture
    }

    @Override
    String getEventId() {
        picture
    }

    @Override
    String getDtoFromEvent() {
        picture
    }
}
