package m30.rest.business.commands

import simple.cqrs.commands.AbstractCommand

class NewGifCommand extends AbstractCommand<String>{

    String fileName

    NewGifCommand(String fileName){
        this.fileName = fileName
    }

}
