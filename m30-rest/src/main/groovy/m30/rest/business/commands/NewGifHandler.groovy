package m30.rest.business.commands

import io.micronaut.http.client.RxHttpClient
import m30.rest.business.GifStorageClient
import m30.rest.business.StorageService
import m30.rest.business.model.M30Picture
import simple.cqrs.commands.AbstractCommandHandler
import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher
import simple.cqrs.model.Command
import simple.cqrs.model.Result

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NewGifHandler extends AbstractCommandHandler<String>{

    @Inject
    GifStorageClient gifStorageClient

    @Inject
    StorageService storageService

    NewGifHandler(EventPublisher<String>event){
        super(event)
    }

    @Override
    String getDto(Command<String> command) {
        NewGifCommand newGifCommand = command as NewGifCommand
        newGifCommand.fileName
    }

    @Override
    Result<String> buildResult(String dto) {
        null
    }

    @Override
    AbstractEvent<String> buildEvent(String dto) {
        gifStorageClient.retrieveImage(dto).doOnSuccess{ M30Picture picture ->
            storageService.save(picture).subscribe()
        }.subscribe()
        return null
    }
}
