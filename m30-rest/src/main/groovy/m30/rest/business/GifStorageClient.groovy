package m30.rest.business

import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client
import io.reactivex.Single
import m30.rest.business.model.M30Picture

@Client('${m30.gif.url}')
interface GifStorageClient {

    @Get('/')
    Single<List<String>> retrieveAllImages( )

    @Get('/{id}')
    Single<M30Picture> retrieveImage(String id )

}
