package m30.rest.business.services

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import io.micronaut.context.annotation.Value
import io.reactivex.Maybe
import io.reactivex.MaybeEmitter
import m30.rest.business.StorageService
import m30.rest.business.model.M30Picture

import javax.inject.Singleton

@Singleton
class FileStorageService implements StorageService{

    @Value('${m30.storage.path}')
    String storagePath

    @Override
    @CompileStatic(TypeCheckingMode.SKIP)
    Maybe<M30Picture> save(M30Picture m30Picture) {
        Maybe.create{ MaybeEmitter emitter ->
            try {
                File dir = new File("$storagePath")
                dir.mkdirs()
                File gif = new File( dir, "${m30Picture.name}.gif")
                gif.bytes = m30Picture.image

                emitter.onComplete()
            }catch( e ){
                emitter.onError(e)
            }
        }
    }
}
