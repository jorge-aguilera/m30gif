package m30.rest.business.services

import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Requires
import io.micronaut.context.env.Environment
import io.micronaut.scheduling.annotation.Scheduled
import io.reactivex.Single
import m30.rest.business.GifStorageClient
import m30.rest.business.commands.NewGifCommand
import simple.cqrs.bus.Bus

import javax.inject.Inject
import javax.inject.Singleton

@Slf4j
@Singleton
@Requires(notEnv = Environment.TEST)
class JobRefresh {

    @Inject
    Bus bus

    @Inject
    GifStorageClient gifStorageClient

    @Scheduled(fixedDelay = '${m30.gif.refresh}', initialDelay = '3s')
    void executeEveryTen() {
        gifStorageClient.retrieveAllImages().doOnSuccess{ List<String> list ->
            list.each {
                NewGifCommand cmd = new NewGifCommand(it)
                bus.handleCommand(cmd)
            }
        }.subscribe()
    }

}
