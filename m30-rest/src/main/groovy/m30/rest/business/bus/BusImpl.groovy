package m30.rest.business.bus

import m30.rest.business.commands.NewGifCommand
import m30.rest.business.commands.NewGifHandler

import simple.cqrs.bus.AbstractBus
import simple.cqrs.model.CommandHandler

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BusImpl extends AbstractBus{

    Map<String, CommandHandler> handlers = [:]


    @Inject
    BusImpl(NewGifHandler newPictureHandler){
        handlers[ NewGifCommand.class.simpleName] = newPictureHandler
    }
}
