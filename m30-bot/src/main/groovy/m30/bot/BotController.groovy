package m30.bot

import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.reactivex.Single
import m30.bot.commands.UserCommand
import m30.bot.telegram.Message
import m30.bot.telegram.SendFile
import m30.bot.telegram.TelegramApiClient
import m30.bot.telegram.Update
import simple.cqrs.bus.Bus

import javax.inject.Inject

@Controller('/${bot.token.prefix}${bot.token.sufix}')
public class BotController {

    @Inject
    Bus bus

    @Post("/")
    String onMessage(@Body final Update update){
        bus.handleCommand( new UserCommand(update) )
        "ok"
    }

    @Inject
    InfoService infoService

    @Get('/info')
    Single<String> currentStatus(){
        infoService.currentStatus()
    }

    @Get(value='/search', produces = 'image/jpg')
    Single<byte[]> search(@QueryValue('search')String search){
        Single.create { emitter->
            infoService.searchCamara(search).doOnSuccess{
                emitter.onSuccess(it.image)
            }
        }
    }

    @Get(value='/list')
    Single<List<String>> list(){
        infoService.listCamaras()
    }

}