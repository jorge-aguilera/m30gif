package m30.bot.service

import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Requires
import io.micronaut.context.annotation.Value
import io.micronaut.context.env.Environment
import io.micronaut.scheduling.annotation.Scheduled
import m30.bot.GifService
import m30.bot.InfoService

import javax.inject.Inject
import javax.inject.Singleton;

@Slf4j
@Singleton
@Requires(notEnv = Environment.TEST)
class Jobs {

    @Inject
    GifService gifService

    @Inject
    InfoService infoService

    @Scheduled(fixedDelay = '${extractor.every:10m}', initialDelay = '10s')
    void renewM30() {
        gifService.renewImage()
    }

    @Scheduled(fixedDelay = '${extractor.every:10m}', initialDelay = '13s')
    void renewA6() {
        gifService.renewA6()
    }

    @Scheduled(fixedDelay = '${extractor.every:10m}', initialDelay = '5s')
    void downloadCamarasMadrid() {
        infoService.downloadCamarasMadrid()
    }

}
