package m30.bot.service

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Slf4j
import io.micronaut.http.client.annotation.Client
import io.reactivex.Single
import m30.bot.InfoService
import org.codehaus.groovy.runtime.MethodRankHelper

import javax.inject.Singleton

@Slf4j
@Singleton
@CompileStatic(TypeCheckingMode.SKIP)
class XmlInfoService implements InfoService{

    String str

    def kml

    File root = new File('build/camaras')

    XmlInfoService() {
        str = "http://informo.munimadrid.es/informo/tmadrid/CCTV.kml".toURL().text.substring(1)
        kml = new XmlSlurper().parseText(str).declareNamespace("xmlns":"http://earth.google.com/kml/2.2")
    }

    @Override
    Single<String> currentStatus() {
        Single.create { emitter ->
            def DatosTrafico = new XmlSlurper().parse("https://www.mc30.es/images/xml/DatosTrafico.xml")
            def datosGlobales = DatosTrafico.DatoGlobal
            def totalVehiculosTunel = datosGlobales.find { it.Nombre.text() == 'totalVehiculosTunel' }.VALOR.text()
            def totalVehiculosCalle30 = datosGlobales.find { it.Nombre.text() == 'totalVehiculosCalle30' }.VALOR.text()
            def velocidadMediaTunel = datosGlobales.find { it.Nombre.text() == 'velocidadMediaTunel' }.VALOR.text()
            def velicidadMediaSuperfice = datosGlobales.find {
                it.Nombre.text() == 'velicidadMediaSuperfice'
            }.VALOR.text()

            String currentStatus = """
Velocidad Media Tunel: ${velocidadMediaTunel}
Velocidad Media Superficie: ${velicidadMediaSuperfice}
Total Vehiculos M30: ${totalVehiculosCalle30}
"""
            emitter.onSuccess(currentStatus)
        }
    }



    @Override
    Single<SearchCamaraResult> searchCamara(final String search) {
        Single.create{ emitter->
            String searchUpperCase = search.toUpperCase()

            List<String>places = root.list(new FilenameFilter() {
                @Override
                boolean accept(File dir, String name) {
                    name.indexOf(searchUpperCase) != -1
                }
            })

            SearchCamaraResult camaraResult = new SearchCamaraResult(search:search)

            if( places ){
                camaraResult.name = places[0]
                camaraResult.image = new File(root, places[0]).bytes
                camaraResult.alternates = places.drop(1).take(5)
            }
            emitter.onSuccess( camaraResult )
        }
    }

    @Override
    Single<List<String>> listCamaras() {
        Single.create{ emitter->
            List<String> ret = []
            def document = kml.Document
            document.Placemark.each{
                ret.add it.ExtendedData.Data[1].Value.text()
            }
            ret = ret.sort()
            emitter.onSuccess(ret)
        }
    }

    void downloadCamarasMadrid(){
        root.mkdirs()
        kml.Document.Placemark.each{
            String name = it.ExtendedData.Data[1].Value.text()
            String description = it.description.text()
            String url = description.split(' ').find{ it.startsWith('src=')}.substring(4)
            File img = new File(root, name.toUpperCase())
            img.bytes = url.toURL().bytes
        }
    }
}
