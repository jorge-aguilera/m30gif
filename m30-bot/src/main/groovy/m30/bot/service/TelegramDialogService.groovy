package m30.bot.service

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Value
import io.micronaut.http.MediaType
import io.micronaut.http.client.multipart.MultipartBody
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.Consumer
import m30.bot.DialogService
import m30.bot.GifService
import m30.bot.InfoService
import m30.bot.telegram.Message
import m30.bot.telegram.Response
import m30.bot.telegram.ResponseMessage
import m30.bot.telegram.SendFile
import m30.bot.telegram.TelegramApiClient
import m30.bot.telegram.Update

import javax.inject.Inject
import javax.inject.Singleton

@Slf4j
@Singleton
class TelegramDialogService implements DialogService{

    @Inject
    GifService gifService

    @Inject
    InfoService infoService

    @Inject
    TelegramApiClient telegramApiClient

    @Override
    Single<Response> processMessageFromUser(Update update) {

        switch( update.message.text ){
            case '/start':
                return buildStart(update.message.chat.id)
            case '/m30':
                return buildM30(update.message.chat.id)
            case '/a6':
                return buildA6(update.message.chat.id)
            case '/status':
                return buildM30Status(update.message.chat.id)
            case '/camaras':
                return buildCamaras(update.message.chat.id)
            default:
                return buildSearch(update.message.chat.id, update.message.text)
        }

    }

    Single<Response> buildStart(String chatId){
        Message msg = new Message(chat_id: chatId, text: """
Hola. Soy un simple bot que intenta mostrarte el estado de las cámaras de tráfico de Madrid
Estos son los comandos que tengo actualmente implementados:

- /m30  (te enviaré un gif animado con las fotos de la m30)
- /a6  (te enviaré un gif animado con las fotos de la A6-Madrid)
- /status (te enviaré un resumen del estado: velocidad media, vehiculos circulando...)
- /camaras (te muestro un listado con todas las calles donde hay cámaras)

Cualquier otro texto intentaré buscar una calle que se parezca y enviarte su estado

Made with ❤ by PuraVida Software
""")
        telegramApiClient.sendMessage(msg)
    }

    Single<Response> buildM30Status(String chatId){
        Single.create{ emitter->
            infoService.currentStatus().doOnSuccess{ String status ->

                Message msg = new Message(chat_id:chatId, text: status )

                telegramApiClient.sendMessage(msg).subscribe()

            }.subscribe()
        }
    }


    Single<Response> buildM30(String chatId) {

        MultipartBody video = MultipartBody.builder()
                .addPart("chat_id", chatId)
                .addPart("animation", 'current.gif', gifService.currentFile )
                .build()

        telegramApiClient.sendVideo(video).doOnSuccess{

            infoService.currentStatus().doOnSuccess{ String status ->

                Message msg = new Message(chat_id:chatId, text: status )

                telegramApiClient.sendMessage(msg).subscribe()

            }.subscribe()
        }
    }

    Single<Response> buildSearch(final String chatId, final String search) {

        Single.create{ emitter->

            if( search.length() < 4){
                Message msg = new Message(chat_id:chatId, text: "Dame más datos que con eso no hago mucho" )
                return telegramApiClient.sendMessage(msg).subscribe()
            }

            infoService.searchCamara(search).doOnSuccess{ InfoService.SearchCamaraResult searchCamaraResult->

                if( !searchCamaraResult.name ){
                    String txt = "Upsss no hay nada con esa busqueda\nRecuerda que con /camaras puedes ver la lista completa de ubicaciones"
                    Message msg = new Message(chat_id:chatId, text: txt)
                    return telegramApiClient.sendMessage(msg).subscribe()
                }

                MultipartBody photo = MultipartBody.builder()
                        .addPart("chat_id", chatId)
                        .addPart("photo", "${searchCamaraResult.name}.jpg", searchCamaraResult.image)
                        .addPart("caption",searchCamaraResult.name)
                        .build()

                telegramApiClient.sendPhoto(photo).doOnSuccess({ onSuccess->
                    if( searchCamaraResult.alternates ){
                        List<String> txt = ["Calles similares:"]
                        txt.addAll searchCamaraResult.alternates.take(3)
                        Message msg = new Message(chat_id:chatId, text: String.join('\n', txt))
                        telegramApiClient.sendMessage(msg).subscribe()
                    }
                } as Consumer).subscribe()

            }.subscribe()
        }
    }

    Single<Response> buildCamaras(String chatId){
        Single.create{ emitter->
            infoService.listCamaras().doOnSuccess{ List<String> camaras->
                try {
                    List<String> ret = ["*Ubicaciones de las cámaras*"]
                    camaras.collate(6).eachWithIndex { group , idx->

                        if( idx ) ret.clear()

                        group.each {
                            ret.add "- $it".toString()
                        }

                        String txt = String.join('\n', ret as String[])

                        Message msg = new Message(chat_id: chatId, text: txt)

                        telegramApiClient.sendMessage(msg).subscribe()
                    }
                }catch(Throwable e){
                    e.printStackTrace()

                    Message msg = new Message(chat_id: chatId, text: "mierrrcoles algo ha ido mal")

                    telegramApiClient.sendMessage(msg).subscribe()
                }

            }.subscribe()
        }
    }

    Single<Response> buildA6(String chatId) {

        MultipartBody video = MultipartBody.builder()
                .addPart("chat_id", chatId)
                .addPart("animation", 'current.gif', gifService.currentA6 )
                .build()

        telegramApiClient.sendVideo(video).doOnSuccess{

            infoService.currentStatus().subscribe()
        }
    }

}
