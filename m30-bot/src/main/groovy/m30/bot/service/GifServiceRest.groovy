package m30.bot.service

import com.puravida.gif.GifGenerator
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import io.micronaut.context.annotation.Value
import m30.bot.GifService
import m30.bot.InfoService

import javax.imageio.ImageIO
import javax.imageio.stream.FileImageOutputStream
import javax.imageio.stream.ImageOutputStream
import javax.inject.Inject
import javax.inject.Singleton
import java.awt.image.BufferedImage

@Singleton
@CompileStatic(TypeCheckingMode.SKIP)
class GifServiceRest implements GifService{

    @Value('${extractor.url}')
    String xmlUrl

    File root = new File('build/m30')

    @Value('${a6.url}')
    String a6url

    @Value('${a6.images}')
    List<String> a6Images = []

    File rootA6 = new File('build/a6')

    GifGenerator gifGenerator = new GifGenerator()

    @Override
    void renewImage() {

        cleanDir(root)

        def Camaras = new XmlParser().parse(xmlUrl).Camara
        List fotos = listFotos(Camaras)

        List<File> files = downloadFotos(fotos)

        new File(root, "m30.gif").bytes = gifGenerator.compose(files).bytes
    }

    void cleanDir(File dir) {
        dir.mkdirs()
        dir.listFiles().each{
            if( it.name.endsWith('.jpg'))
                it.delete()
        }
    }

    List listFotos( Camaras ) {
        Camaras.findAll {
            "${it.Nombre.text()}".startsWith("M30-PK")
        }.sort{
            "${it.Nombre.text()}"
        }
    }

    List<File> downloadFotos( list ) {
        List<File> files = []
        list.eachWithIndex { item, idx ->
            byte[] img = ("http://" + item.URL.text()).toURL().bytes
            File f = new File(root, "${idx}.jpg")
            f.bytes = img
            files.add f
        }
        files
    }



    @Override
    File getCurrentFile() {
        root.mkdirs()
        File gif = new File(root, "m30.gif")
        gif
    }


    @Override
    void renewA6() {

        cleanDir(rootA6)

        List<File> files = []
        a6Images.eachWithIndex { item, idx ->
            byte[] img = ("$a6url/${item}.jpg").toURL().bytes
            File f = new File(rootA6, "${idx}.jpg")
            f.bytes = img
            files.add f
        }

        new File(rootA6, "a6.gif").bytes = gifGenerator.compose(files).bytes
    }

    @Override
    File getCurrentA6() {
        new File(rootA6, "a6.gif")
    }

}
