package m30.bot.telegram

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.multipart.MultipartBody
import io.reactivex.Maybe
import io.reactivex.Single

@Client('${bot.api.full}')
interface TelegramApiClient {

    @Post("/setWebhook")
    Single<Response<Object>> setWebhook(@QueryValue(value="url")String setWebhook)

    @Post("/getMe")
    Single<Response<Me>> getMe()

    @Post("/sendMessage")
    Single<Response<ResponseMessage>> sendMessage(@Body Message message)

    @Post(value="/sendAnimation", produces = [MediaType.MULTIPART_FORM_DATA])
    Single<Response<ResponseMessage>> sendVideo(@Body MultipartBody message)

    @Post(value="/sendPhoto", produces = [MediaType.MULTIPART_FORM_DATA])
    Single<Response<ResponseMessage>> sendPhoto(@Body MultipartBody message)
}
