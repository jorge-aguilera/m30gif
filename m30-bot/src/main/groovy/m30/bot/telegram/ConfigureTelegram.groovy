package m30.bot.telegram

import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Requires
import io.micronaut.context.annotation.Value
import io.micronaut.context.env.Environment
import io.micronaut.context.event.ApplicationEventListener
import io.micronaut.discovery.event.ServiceStartedEvent
import m30.bot.telegram.TelegramApiClient

import javax.inject.Inject
import javax.inject.Singleton

@Slf4j
@Singleton
@Requires(notEnv = Environment.TEST)
class ConfigureTelegram implements ApplicationEventListener<ServiceStartedEvent> {

    @Inject
    TelegramApiClient telegramApiClient

    @Value('${bot.url}')
    String botUrl

    @Value('${bot.token.prefix}')
    String botTokenPrefix

    @Value('${bot.token.sufix}')
    String botTokenSufix

    @Override
    public void onApplicationEvent(ServiceStartedEvent event) {
        String setWebhook = "$botUrl/$botTokenPrefix$botTokenSufix"
        telegramApiClient.setWebhook(setWebhook).doOnSuccess { obj ->
            telegramApiClient.getMe().doOnSuccess {
                log.info "$it"
            }.subscribe()
        }.subscribe();
    }


}
