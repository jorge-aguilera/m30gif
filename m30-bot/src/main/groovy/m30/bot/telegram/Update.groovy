package m30.bot.telegram

import groovy.transform.ToString

@ToString
class Update {

    int update_id;

    ResponseMessage message;

}
