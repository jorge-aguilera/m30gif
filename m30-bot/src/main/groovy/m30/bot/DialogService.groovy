package m30.bot

import io.reactivex.Maybe
import io.reactivex.Single
import m30.bot.telegram.Response
import m30.bot.telegram.ResponseMessage
import m30.bot.telegram.Update

interface DialogService {

    Single<Response> processMessageFromUser(Update message)

}
