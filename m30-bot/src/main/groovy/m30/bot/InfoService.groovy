package m30.bot

import io.reactivex.Single

interface InfoService {

    Single<String> currentStatus()

    Single<List<String>> listCamaras()

    void downloadCamarasMadrid()

    class SearchCamaraResult{
        String search
        String name
        byte[] image
        List<String> alternates
    }

    Single<SearchCamaraResult> searchCamara(String search)
}