package m30.bot

interface GifService {

    File getCurrentFile()

    void renewImage()

    void renewA6()

    File getCurrentA6()
}
