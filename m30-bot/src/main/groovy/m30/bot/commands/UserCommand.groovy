package m30.bot.commands

import m30.bot.telegram.Update
import simple.cqrs.commands.AbstractCommand

class UserCommand extends AbstractCommand<Update>{

    Update update
    UserCommand(Update update){
        this.update = update
    }
}
