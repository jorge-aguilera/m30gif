package m30.bot.commands

import groovy.util.logging.Slf4j
import m30.bot.event.UserCommandEvent
import m30.bot.telegram.Update
import simple.cqrs.commands.AbstractCommandHandler
import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher
import simple.cqrs.model.Command
import simple.cqrs.model.Result

import javax.inject.Singleton

@Slf4j
@Singleton
class UserCommandHandler extends AbstractCommandHandler<Update>{

    UserCommandHandler(EventPublisher<Update> eventPublisher){
        super(eventPublisher)
    }

    @Override
    Update getDto(Command<Update> command) {
        (command as UserCommand).update
    }

    @Override
    AbstractEvent<Update> buildEvent(Update dto) {
        log.info "received ${dto?.message?.text} from ${dto?.message?.from}"
        new UserCommandEvent(dto)
    }

    @Override
    Result<Update> buildResult(Update dto) {
        new Result<Update>(){}
    }
}
