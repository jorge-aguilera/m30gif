package m30.bot.event

import io.micronaut.runtime.event.annotation.EventListener
import io.micronaut.scheduling.annotation.Async
import io.reactivex.Maybe
import m30.bot.DialogService
import m30.bot.GifService
import m30.bot.telegram.Response

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserComandEventListener {

    @Inject
    DialogService dialogService

    @Async
    @EventListener
    void onUserCommandEvent(UserCommandEvent event){
        dialogService.processMessageFromUser(event.update).subscribe()
    }

}
