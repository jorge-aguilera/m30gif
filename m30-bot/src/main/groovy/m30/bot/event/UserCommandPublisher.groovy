package m30.bot.event

import io.micronaut.context.annotation.Primary
import io.micronaut.context.event.ApplicationEventPublisher
import m30.bot.commands.UserCommand
import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher

import javax.inject.Inject
import javax.inject.Singleton

@Primary
class UserCommandPublisher implements EventPublisher<UserCommand> {

    @Inject
    ApplicationEventPublisher eventPublisher

    @Override
    void publish(AbstractEvent<UserCommand> event) {
        eventPublisher.publishEvent(event)
    }
}
