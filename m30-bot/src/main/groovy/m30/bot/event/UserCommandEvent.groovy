package m30.bot.event

import m30.bot.telegram.Update
import simple.cqrs.event.AbstractEvent

class UserCommandEvent  extends AbstractEvent<Update> implements  Serializable{

    Update update
    UserCommandEvent(){

    }

    UserCommandEvent(Update update){
        this.update = update
    }

    @Override
    String getEventId() {
        update.update_id
    }

    @Override
    Update getDtoFromEvent() {
        update
    }
}
