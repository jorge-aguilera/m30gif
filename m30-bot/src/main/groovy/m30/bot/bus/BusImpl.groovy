package m30.bot.bus

import m30.bot.commands.UserCommand
import m30.bot.telegram.Update
import simple.cqrs.bus.AbstractBus
import simple.cqrs.model.Command
import simple.cqrs.model.CommandHandler

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BusImpl extends AbstractBus{

    Map<String, CommandHandler<Command<Update>, Update>> handlers = [:]


    @Inject
    BusImpl(CommandHandler<Command<Update>, Update> handler){
        handlers[UserCommand.class.simpleName] = handler
    }

}
