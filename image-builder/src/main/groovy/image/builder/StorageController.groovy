package image.builder

import image.builder.business.model.M30Gif
import image.builder.business.model.M30Picture
import io.micronaut.context.annotation.Value
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Single

@Controller('/')
class StorageController {

    @Value('${m30.storage.path}')
    String storagePath

    @Get('/')
    Single<List<String>>index(){
        Single.create{ emitter ->

            File root = new File(storagePath)
            root.mkdirs()
            def pattern = ~/.*\.gif/

            List<String> list = []
            root.eachFileMatch pattern, {
                list.add it.name.md5()
            }
            list.sort{ it }

            emitter.onSuccess(list)
        }
    }


    @Get(value='/{id}')
    Single<M30Gif> file(String id){
        Single.create{ emitter->
            File root = new File(storagePath)
            def pattern = ~/.*\.gif/

            File file
            root.eachFileMatch pattern, {
                if( it.name.md5() == id ){
                    file = it
                }
            }

            if( !file )
                emitter.onError(new FileNotFoundException())

            M30Gif ret = new M30Gif(
                    name: file.name.split('\\.').dropRight(1).join(' '),
                    image: file.bytes
            )

            emitter.onSuccess(ret)
        }
    }

}
