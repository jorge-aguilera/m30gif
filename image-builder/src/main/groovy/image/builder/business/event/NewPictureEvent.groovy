package image.builder.business.event

import image.builder.business.model.M30Picture
import simple.cqrs.event.AbstractEvent

class NewPictureEvent extends AbstractEvent<String> implements  Serializable{

    String picture

    NewPictureEvent(){
    }

    NewPictureEvent(String picture){
        this.picture = picture
    }

    @Override
    String getEventId() {
        picture
    }

    @Override
    String getDtoFromEvent() {
        picture
    }
}
