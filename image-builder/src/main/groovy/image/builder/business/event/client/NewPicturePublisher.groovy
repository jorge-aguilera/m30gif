package image.builder.business.event.client

import image.builder.business.StorageService
import image.builder.business.model.M30Picture
import io.micronaut.context.annotation.Primary
import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher

import javax.inject.Inject

@Primary
class NewPicturePublisher implements EventPublisher<M30Picture>{

    @Inject
    EventClient<M30Picture> eventClient

    @Override
    void publish(final AbstractEvent<M30Picture> event) {
        eventClient.sendEvent(event.eventId, event.dtoFromEvent)
    }
}
