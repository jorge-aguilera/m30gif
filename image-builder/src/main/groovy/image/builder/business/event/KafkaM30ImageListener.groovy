package image.builder.business.event


import image.builder.business.commands.NewPictureCommand
import image.builder.business.model.M30Picture
import io.micronaut.configuration.kafka.annotation.KafkaListener
import io.micronaut.configuration.kafka.annotation.OffsetReset
import io.micronaut.configuration.kafka.annotation.Topic
import io.reactivex.Single
import simple.cqrs.bus.Bus

import javax.inject.Inject

@KafkaListener(offsetReset = OffsetReset.LATEST, groupId = 'image-builder')
class KafkaM30ImageListener {

    @Inject
    Bus bus

    @Topic('${m30.kafka.topic}')
    Single<M30Picture> receive(Single<M30Picture> m30PictureFlowable){
        m30PictureFlowable.doOnSuccess{ m30Picture->
            NewPictureCommand cmd = new NewPictureCommand(m30Picture)
            bus.handleCommand(cmd)
        }
    }

}
