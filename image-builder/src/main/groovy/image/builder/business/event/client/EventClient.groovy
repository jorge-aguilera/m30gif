package image.builder.business.event.client

import io.micronaut.configuration.kafka.annotation.KafkaClient
import io.micronaut.configuration.kafka.annotation.KafkaKey
import io.micronaut.configuration.kafka.annotation.Topic
import io.micronaut.messaging.annotation.Body

@KafkaClient
interface EventClient<T> {

    @Topic('${m30.kafka.topicgif}')
    void sendEvent(@KafkaKey String id, @Body T body)
}
