package image.builder.business

import image.builder.business.model.M30Picture
import io.reactivex.Maybe

interface ImageService {

    String generate(M30Picture m30Picture)

}
