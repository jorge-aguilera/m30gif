package image.builder.business.services

import groovy.util.logging.Slf4j
import image.builder.business.ImageService
import image.builder.business.StorageService
import image.builder.business.model.M30Picture
import io.micronaut.context.annotation.Value
import io.reactivex.Maybe
import io.reactivex.MaybeEmitter

import javax.inject.Inject
import javax.inject.Singleton

@Slf4j
@Singleton
class FileStorageService implements StorageService{

    @Value('${m30.storage.path}')
    String storagePath

    @Override
    boolean save(final M30Picture m30Picture) {

        File f = new File("$storagePath/${m30Picture.name}/${m30Picture.when.time.time}.jpg")
        f.parentFile.mkdirs()

        List<File> files = f.parentFile.listFiles() as List<File>
        files.sort { it.name.split('\\.').first() as long }

        if(files.size() > 10) {
            files.take(10).each { File remove ->
                remove.delete()
            }
            files = f.parentFile.listFiles() as List<File>
            files.sort { it.name.split('\\.').first() as long }
        }

        byte[] bytes = m30Picture.image

        boolean write = files.size() ? !Arrays.equals( files.last().bytes, bytes) : true

        if (write) {
            f.bytes = bytes
            log.info "nueva foto para $m30Picture.name"
        }

        write
    }
}
