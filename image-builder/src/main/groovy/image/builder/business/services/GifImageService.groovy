package image.builder.business.services

import elliot.kroo.GifSequenceWriter
import image.builder.business.ImageService
import image.builder.business.model.M30Picture
import io.micronaut.context.annotation.Value
import io.reactivex.Maybe
import io.reactivex.MaybeEmitter

import javax.imageio.ImageIO
import javax.imageio.stream.FileImageOutputStream
import javax.imageio.stream.ImageOutputStream
import javax.inject.Singleton
import java.awt.image.BufferedImage
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Singleton
class GifImageService implements ImageService{

    @Value('${m30.storage.path}')
    String storagePath

    @Value('${m30.gif.images}')
    int imagesSize

    @Override
    String generate(final M30Picture m30Picture) {
        File dir = new File("$storagePath/${m30Picture.name}")
        dir.mkdirs()

        List<File> files = dir.listFiles() as List<File>
        files.sort { it.name.split('\\.').first() as long }

        File gif = new File( dir.parentFile, "${m30Picture.name}.gif")
        ImageOutputStream output = new FileImageOutputStream( gif )

        BufferedImage firstImage = ImageIO.read(files.first())

        GifSequenceWriter writer =
                new GifSequenceWriter(output, firstImage.getType(), 1000, true);

        files.takeRight(imagesSize)each {
            BufferedImage image = ImageIO.read(it)
            writer.writeToSequence( image )
        }
        writer.close()
        output.close()

        gif.name.md5()
    }

}
