package image.builder.business.bus


import image.builder.business.commands.NewPictureCommand
import image.builder.business.commands.NewPictureHandler
import image.builder.business.model.M30Picture
import simple.cqrs.bus.AbstractBus
import simple.cqrs.model.Command
import simple.cqrs.model.CommandHandler

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BusImpl extends AbstractBus{

    Map<String, CommandHandler<Command<M30Picture>, M30Picture>> handlers = [:]


    @Inject
    BusImpl(
            NewPictureHandler newPictureHandler
    ){
        handlers[ NewPictureCommand.class.simpleName] = newPictureHandler
    }

}
