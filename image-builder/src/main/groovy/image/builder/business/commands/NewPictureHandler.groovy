package image.builder.business.commands

import image.builder.business.ImageService
import image.builder.business.StorageService
import image.builder.business.event.NewPictureEvent
import image.builder.business.model.M30Picture
import image.builder.business.model.M30PictureResult
import io.reactivex.Single
import simple.cqrs.commands.AbstractCommandHandler
import simple.cqrs.event.AbstractEvent
import simple.cqrs.event.client.EventPublisher
import simple.cqrs.model.Command
import simple.cqrs.model.Result

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NewPictureHandler extends AbstractCommandHandler<M30Picture>{

    @Inject
    StorageService storageService

    @Inject
    ImageService imageService

    NewPictureHandler(EventPublisher<M30Picture> eventPublisher){
        super(eventPublisher)
    }

    @Override
    M30Picture getDto(Command<M30Picture> command) {
        (command as NewPictureCommand).picture
    }

    @Override
    AbstractEvent<String> buildEvent(M30Picture dto) {
        if( !storageService.save(dto) )
            return null

        String fileName = imageService.generate(dto)

        new NewPictureEvent(fileName) as AbstractEvent
    }

    @Override
    Result<NewPictureCommand> buildResult(M30Picture dto) {
        new M30PictureResult() as Result
    }


    @Override
    Result<NewPictureCommand> handleCommand(Command<M30Picture> command) {
        final M30Picture dto = getDto(command)
        Single.create{ emitter->
            NewPictureEvent event = buildEvent(dto) as NewPictureEvent
            if( event ) {
                //publish(event as AbstractEvent<M30Picture>)
                emitter.onSuccess(event.dtoFromEvent)
            }
            emitter
        }.subscribe()
        null
    }

}
