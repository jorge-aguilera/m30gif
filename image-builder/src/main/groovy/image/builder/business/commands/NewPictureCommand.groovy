package image.builder.business.commands

import image.builder.business.model.M30Picture
import simple.cqrs.commands.AbstractCommand

class NewPictureCommand extends AbstractCommand<M30Picture> {

    M30Picture picture

    NewPictureCommand(M30Picture picture){
        this.picture = picture
    }

}
