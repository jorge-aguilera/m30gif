package image.builder.business.model


import groovy.transform.ToString

@ToString
class M30Picture {

    String name

    Calendar when

    byte[] image

    String id

}
