package image.builder.business.model

import groovy.transform.ToString

@ToString
class M30Gif {

    String name
    byte[] image
}
