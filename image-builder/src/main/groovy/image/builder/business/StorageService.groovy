package image.builder.business

import image.builder.business.model.M30Picture
import io.reactivex.Maybe

interface StorageService {

    boolean save(M30Picture m30Picture)

}
